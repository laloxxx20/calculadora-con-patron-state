public interface OperationsStates // abstract class State
{	
	public  int makeOperation(int a, int b);
}