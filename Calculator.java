public class Calculator //like class contexto in State pattern
{
	private OperationsStates toChangueState;

	public Calculator()
	{		
		System.out.println("since Calculator");	
	}

	public void setOperation(OperationsStates ope)
	{
		toChangueState=ope;	
	}

	public int makeOperation(int a, int b) //make the operations when state is selectionated
	{		
		return toChangueState.makeOperation(a,b);		
	}

}