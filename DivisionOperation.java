public class DivisionOperation implements OperationsStates //State
{ 
	public DivisionOperation()
	{
		// System.out.println("since sDivisionOperation");
	}

	// @Override
	public int makeOperation(int a, int b)
	{
		return a/b;
	}
}