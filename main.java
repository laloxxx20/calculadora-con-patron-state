import java.util.Scanner;

public class main  //here we aply the State pattern
{	
		
	public static void main(String[] args) 
	{		
		Calculator calcu=new Calculator();		

		SumOperation sum=new SumOperation();
		SubstractionOperation subs=new SubstractionOperation();
		MultiOperation mul=new MultiOperation();		
		DivisionOperation div=new DivisionOperation();

		/*****(S)test with just one to one STATE*****/
		// calcu.setOperation(div); // here we choose the state in the calculator
		// int rpta=calcu.makeOperation(4,2); // and here we make the operation with tha state  chosen
		// System.out.println(rpta);
		/*****(E)test with just one to one STATE*****/

		/*********** (S)test wiht MENU ************/
		int opcion = 0;			

		do{
			StringBuffer menu = new StringBuffer();
			menu.append("****************************************\n");
			menu.append("* SELECCIONE EL ESTADO(operacion) DE LA CALCULADORA    *\n");
			menu.append("* 1- SUMA. 2- RESTA 3-MULTI 4-DIVISION 0-SALIR *\n");
			menu.append("****************************************\n");						
			System.out.println(menu.toString());

			Scanner sc = new Scanner(System.in);			
			opcion = sc.nextInt();

			StringBuffer parameter = new StringBuffer();
			
			System.out.println("* PARAMETER ONE *\n");
			int one= sc.nextInt();
			
			System.out.println("* PARAMETER TWO *\n");
			int two= sc.nextInt();
			
			switch(opcion){ // here put the state in the calcu
				case 1:
					calcu.setOperation(sum);
					break;
				case 2:
					calcu.setOperation(subs);
					break;
				case 3:
					calcu.setOperation(mul);
					break;
				case 4:
					calcu.setOperation(div);
					break;	
				case 0:
					System.exit(0);
				default:
					System.out.println("Opcion errada");
			}
			int rpta=calcu.makeOperation(one,two);
			System.out.println("* ANSWER: *\n"+rpta);
		} while(opcion!=0);
		
		/*********** (E)test wiht MENU ************/
	}
}