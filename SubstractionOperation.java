public class SubstractionOperation implements OperationsStates //State
{
	public SubstractionOperation()
	{
		// System.out.println("since substractionOperation");
	}

	// @Override
	public int makeOperation(int a, int b)
	{
		return a-b;
	}
}